﻿using System;
namespace GuiaI_LR132112.Complementario
{
    public class Complementario1
    {
        static long n;
        static double[] item;
        static double result;

        static void Main(string[] args){
            initializeProj();
        }

        static void initializeProj()
        {
            Console.Clear();
            Console.WriteLine("\t\t I T E R A T E");

            Console.Write("\n\n\t Insert N: ");
            n = Convert.ToInt64(Console.ReadLine());

            item = new Double[n];

            for (int i = 1; i < n; i++)
            {
                if (i == 1)
                {
                    item[i] = i / Math.Pow(2, i);
                }

                if ((i % 2) == 0)
                {
                    item[i] = item[i - 1] - (i / Math.Pow(2, i));
                }
                else
                {
                    item[i] = item[i - 1] + (i / Math.Pow(2, i));
                }

                result = item[i];
            }

            Console.Write("Result is -> {0} ", result);
              
        }
    }
}
