﻿using System;
namespace GuiaI_LR132112.Complementario
{
    public class Complementario2
    {
        static long n;
        static Double[] students_weight;
        static int summary;

        static void Main(string[] args)
        {
            initializeProj();
        }

        static void initializeProj()
        {
            Console.Clear();
            Console.WriteLine("\t\t I T E R A T E - II");

            Console.Write("\n\n\t Insert # students: ");
            n = Convert.ToInt64(Console.ReadLine());

            students_weight = new Double[n];
            for (int j = 0; j < n; j++)
            {
                Console.Write("\n\t Student[{0}] weigh: ", j);
                students_weight[j] = Double.Parse(Console.ReadLine());
            }
            showAndSum(students_weight);
            Console.ReadKey();

        }

        static void showAndSum(Double[] weight)
        {
            Console.Clear();
            int i = 1;
            foreach (int student_weight in weight)
            {
                summary += student_weight;
                Console.WriteLine("\n\t Student [{0}] -> '{1}", i, student_weight);
                i++;
            }
            Console.WriteLine("\n\n\t Summary of students weight is: {0}",summary);
        }

    }
}
