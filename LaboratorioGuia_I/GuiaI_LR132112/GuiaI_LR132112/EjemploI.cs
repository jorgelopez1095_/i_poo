﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuiaI_LR132112
{
    class EjemploI
    {
        static Double Potencia(Double x)
        {
            return x * x;
        }

        static void Main(string[] args)
        {
            Console.Title = "Potencia con funciones";
            Double x;
            for (x = 1; x <= 10; x++)
            {
                Console.WriteLine("\n El cuadrado de " + x + " es: " + Potencia(x)); 
            }
            Console.ReadKey();
        }
    }
}
