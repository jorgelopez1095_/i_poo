﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuiaI_LR132112
{
    class EjemploII
    {
        static void Raiz()
        {
            Console.WriteLine("Calculando raices");
            for(int i = 1; i <= 10; i++)
            {
                Console.WriteLine("\n La raíz cuadrada del número " + i + " es: " + Math.Sqrt(i));
            }
        }

        static void Main(string[] args)
        {
            Raiz();
            Console.ReadKey();
        }

    }
}
