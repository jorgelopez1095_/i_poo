﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuiaI_LR132112
{
    class EjemploIII
    {
        static void Main(string[] args)
        {
            String opc;
             
            Console.WriteLine("\t \t PROGRAMA PARA CONVERTIR TEMPERATURAS");
            Console.WriteLine("\n Elige el tipo de conversion");
            Console.WriteLine("\n\n Presiona F|f si quieres convertir Celsius -> Farenheitr");
            Console.WriteLine("\n\n Presiona C|c si quieres convertir Farenheit -> Celsius");
            Console.WriteLine("\n Ingresa una opcion: ");

            opc  = Console.ReadLine();

            switch (opc)
            {
                case "F":
                case "f":
                    Console.WriteLine("\n Conversion a F");
                    Faren();
                    break;
                case "C":
                case "c":
                    Console.WriteLine("\n Conversion a C°");
                    Celsius();
                    break;
                default:
                    Console.WriteLine("\n Tipo de conversión no válida");
                    break;
            }

            Console.ReadKey();
        }

        static void Faren()
        {
            Double cel, far;
            Console.Write("Escribe los grados celsius: ");

            cel = Convert.ToDouble(Console.ReadLine());
            far = cel * 9.0 / 5.0 + 32;

            Console.WriteLine("{0} grados Celsius son {1} grados Farenheit", cel, far);
        }

        static void Celsius()
        {
            Double cel, far;
            Console.Write("Escribe los grados en Farenheit: ");
            far = Convert.ToDouble(Console.ReadLine());
            cel = (far - 32) * 5.0 / 9.0;
            Console.WriteLine("{0} grados Farnheit son {1} grados Celsius", far, cel);
        }
    }
}