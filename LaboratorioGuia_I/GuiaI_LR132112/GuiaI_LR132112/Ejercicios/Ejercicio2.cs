﻿using System;
namespace GuiaI_LR132112.Ejercicios
{
    class Ejercicio2
    {
        static int i = 0;
        static int age = 0;
        static String user_name;
        static String year_of_birth = "";

        static int current_year = DateTime.Now.Year;

        static void Main(string[] args)
        {
            initializeProj();

        }

        static void initializeProj(){
            Console.WriteLine("CALCULATE YOUR AGE!");
            Console.Write("\n\nYour name: ");
            user_name = Console.ReadLine();

            if(user_name.Length == 0){
                user_name = "Dummy";
            }

            do
            {
                if(validate_year(year_of_birth)){
                    
                    if(i != 0)
                    {
                        Console.Clear();
                        Console.Write("\n{0}, please insert a correct value!", user_name);
                    }

                    Console.Write("\nInsert your year of birth [YYYY]: ");

                    year_of_birth   = Console.ReadLine();

                    i++;

                } else break;

            } while (validate_year(year_of_birth) == true);

            calculateAge(user_name, year_of_birth);

        }


        static Boolean validate_year(String size_of_string)
        {
            if(size_of_string.Length == 0               || 
               size_of_string.Length > 4                || 
               size_of_string.Length < 4                ||
               Convert.ToInt32(size_of_string) <= 1917  ||
               Convert.ToInt32(size_of_string) >= current_year)
            {
                return true;   
            }
            return false;
        }


        static void calculateAge(String name, String year)
        {
            Console.Clear();
            if(Convert.ToInt32(year) == (current_year - 1))
            {
                Console.Write("\n\n\n\t\t\tWelcome {0}, probably you have a: \n\n " +
                              "- couple of months \n " +
                              "- you will soon be one year old or you have a year. \n\n", name);
            }else{
                age = current_year - Convert.ToInt32(year);
                Console.Write("\n\n\n\t\t\tWelcome {0}, your age is: {1}", name, age);   
            }
        }

    }
}
