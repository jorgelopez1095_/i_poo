﻿using System;
namespace GuiaI_LR132112.Ejercicios
{
    public class Ejercicio3
    {

        static String option, divider_temp;
        static int divider = 0, i = 0, dividend;

        static double division_result, cube, height,weight, imc;


        static void Main(string[] args)
        {
            initializeProj();   
        }


        static void initializeProj(){
            Console.Clear();
            Console.WriteLine("\t \t MULTIPLE OPERATIONS");
            Console.WriteLine("\n\n Divide        [D|d]");
            Console.WriteLine("\n\n Get cube      [C|c]");
            Console.WriteLine("\n\n Calcule IMC   [I|i]");
            Console.WriteLine("\n\n Exit          [X|x]");

            Console.Write("\n Insert option: ");

            option = Console.ReadLine();

            switch (option)
            {
                case "D":
                case "d":
                    doDivision();
                    break;
                case "C":
                case "c":
                    getCube();
                    break;
                case "I":
                case "i":
                    calculateIMC();
                    break;
                case "X":
                case "x":
                    break;
                default:
                    Console.WriteLine("\n Type not valid.");
                    break;
            }
        }


        static void showLastMenu(){
            Console.WriteLine("\n\t S U C C E S S!");
            Console.WriteLine("\n\n Back to menu  [M|m]");
            Console.WriteLine("\n Exit          [X|x]");

            Console.Write("\n Insert option: ");

            option = Console.ReadLine();

            switch(option){
                case "m":
                case "M":
                    initializeProj();
                    break;
                case "X":
                case "x":
                    break;
                default:
                    Console.WriteLine("\n Type not valid.");
                    break;
            }
        }


        static void doDivision()
        {
            Console.Clear();
            Console.WriteLine("\t\t D I V I S I O N");

            Console.Write("\n\n\t Insert dividend: ");
            dividend    = Convert.ToInt32(Console.ReadLine());

            do
            {
                if(divider == 0){

                    if(i != 0)
                    {
                        Console.Write("\t Division by zero/null is undefined\n");
                    }

                    Console.Write("\n\t Insert divider: ");
                    divider_temp = Console.ReadLine();

                    if(divider_temp.Length != 0){

                        if(divider_temp != "0") break;

                    } else divider = 0;

                    i++;
                }

                else break;

            } while (divider == 0);

            divider         = Convert.ToInt32(divider_temp);
            division_result = dividend / divider;
            Console.Write("\n\n\t {0} / {1} = {2}", dividend, divider, division_result);
            Console.ReadKey();
            showLastMenu(); 

        }


        static void getCube()
        {
            Console.Clear();
            Console.WriteLine("\t\t C U B E");

            Console.Write("\n\n\t Insert integer value: ");
            cube = Math.Pow(Convert.ToDouble(Console.ReadLine()), 3);

            Console.Write("\n\n\t The cube is -> {0}", cube);
            Console.ReadKey();

            showLastMenu();
        }


        static void calculateIMC()
        {
            Console.Clear();
            Console.WriteLine("\t\t I M C");

            Console.Write("\n\n\t Insert your weight: ");
            weight = Convert.ToDouble(Console.ReadLine());

            Console.Write("\n\n\t Insert your height: ");
            height = Convert.ToDouble(Console.ReadLine());

            imc = weight / Math.Pow(height, 2);

            Console.Write("\n\n\t Your IMC is -> {0} ", imc);
            Console.ReadKey();
            showLastMenu();
        }

    }
}
