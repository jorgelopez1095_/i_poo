﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuiaI_LR132112.Ejercicios
{
    class EjercicioI
    {
        static Double celsius, farenheit, kelvin;



        /** Farenheit **/



        static void Farenheit()
        {
            Console.WriteLine("\t ¿Celsius -> Farenheit?    [C]");
            Console.WriteLine("\t ¿Kelvin  -> Farenheit?    [K]");

            switch (Console.ReadLine())
            {
                case "C":
                case "c":
                    Console.Write("Insert Celsius: ");
                    celsius     = Convert.ToDouble(Console.ReadLine());
                    farenheit   = celsius * 9.0 / 5.0 + 32;
                    Console.WriteLine("{0} Celsius = {1} Farenheit", celsius, farenheit);
                    break;
                case "k":
                case "K":
                    Console.Write("Insert Kelvin: ");
                    kelvin      = Convert.ToDouble(Console.ReadLine());
                    farenheit   = (kelvin - 273.15) * 1.8000 + 32.00;
                    Console.WriteLine("{0} Kelvin = {1} Farenheit", kelvin, farenheit);
                    break;
                default:
                    Console.WriteLine("\n Type not valid.");
                    break;
            }
            Console.ReadKey();
        }



        /** Celsius **/



        static void Celsius()
        {
            Console.WriteLine("\t ¿Kelvin       -> Celsius?    [K]");
            Console.WriteLine("\t ¿Farenheit    -> Celsius?    [F]");

            switch (Console.ReadLine())
            {
                case "K":
                case "k":
                    Console.Write("Insert Kelvin: ");
                    kelvin = Convert.ToDouble(Console.ReadLine());
                    celsius = kelvin - 273.15;
                    Console.WriteLine("{0} Kelvin = {1} Celsius", kelvin, celsius);
                    break;
                case "f":
                case "F":
                    Console.Write("Insert Farenheit: ");
                    farenheit   = Convert.ToDouble(Console.ReadLine());
                    celsius     = (farenheit - 32) * 5.0 / 9.0;
                    Console.WriteLine("{0} Farenheit = {1} Celsius", farenheit, celsius);
                    break;
                default:
                    Console.WriteLine("\n Type not valid.");
                    break;
                    
            }
            Console.ReadKey();
        }



        /** Kelvin **/



        static void Kelvin()
        {
            Console.WriteLine("\t ¿Celsius      -> kelvin?    [C]");
            Console.WriteLine("\t ¿Farenheit    -> Kelvin?    [F]");

            switch (Console.ReadLine())
            {
                case "C":
                case "c":
                    Console.Write("Insert Celsius: ");
                    celsius = Convert.ToDouble(Console.ReadLine());
                    kelvin = celsius + 273.15;
                    Console.WriteLine("{0} Celsius = {1} Kelvin", celsius, kelvin);
                    break;
                case "F":
                case "f":
                    Console.Write("Insert Farenheit: ");
                    farenheit   = Convert.ToDouble(Console.ReadLine());
                    kelvin = (kelvin + 459.67) * 5 / 9;
                    Console.WriteLine("{0} Farenheit = {1} Kelvin", farenheit, kelvin);
                    break;
                default:
                    Console.WriteLine("\n Type not valid.");
                    break;
            }
        }




        static void Main(string[] args)
        {
            String opc;

            Console.WriteLine("\t \t CONVERT TEMPERATURES");
            Console.WriteLine("\n\n Convert to Farenheit        [F|f]");
            Console.WriteLine("\n\n Convert to Celsius          [C|c]");
            Console.WriteLine("\n\n Convert to Kelvin           [K|k]");

            Console.WriteLine("\n Insert option: ");

            opc = Console.ReadLine();

            switch (opc)
            {
                case "F":
                case "f":
                    Farenheit();
                    break;
                case "C":
                case "c":
                    Celsius();
                    break;
                case "K":
                case "k":
                    Kelvin();
                    break;
                default:
                    Console.WriteLine("\n Type not valid.");
                    break;
            }

            Console.ReadKey();
        }

    }
}